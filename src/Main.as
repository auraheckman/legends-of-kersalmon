package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import game.World1;
	import net.flashpunk.*;

	/**
	 * ...
	 * @author Nick Heckman
	 */
	[Frame(factoryClass="Preloader")]
	public class Main extends Engine 
	{

		public function Main():void 
		{
			super(800, 600);
			FP.screen.scale = 2;
			FP.world = new World1("home", 128, 32*9);
			
			FP.screen.color = 0x000000;
		}
		
		override public function init():void
		{
			
		}

	}

}