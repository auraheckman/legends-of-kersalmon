package game 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Spritemap;
	/**
	 * ...
	 * @author ...
	 */
	public class Octoninja extends BaseEntity
	{
		[Embed(source = "../Assets/Octoninja.png")]
		private const OCTONINJA:Class;
		
		private var hideTimer:Number;
		private var damageTime:Number;
		private var hitDirection:int;
		private var deathTimer:Number;
		private var attackTimer:Number;
		
		private var animation:Spritemap;
		
		public function Octoninja() 
		{
			animation = new Spritemap(OCTONINJA, 128, 64);
			animation.add("stand", [0], 10);
			animation.add("attack", [1, 2, 2, 2, 0], 10, false);
			animation.add("escape", [3, 4, 5, 6, 7, 8], 10, false);
			animation.add("return", [7, 6, 5, 4, 3, 0], 10, false);
			addGraphic(animation);
			animation.originX = 64;
			animation.originY = 64;
			
			setHitbox(28, 46, 14, 46);
			
			hideTimer = 0;
			deathTimer = 0;
			attackTimer = 0;
			damageTime = 0;
			
			health = 25;
			
			type = "enemy";
		}
		
		override public function update():void 
		{
			super.update();
			
			if (health > 0)
			{
				if (hideTimer <= 0 && Math.abs((FP.world as BaseWorld).player.x - x) < 96)
				{
					hideTimer = 1.2;
					animation.play("escape");
				}
				
				if (damageTime <= 0 && hideTimer <= 0 && onCamera)
				{
					attackTimer += FP.elapsed;
					if (attackTimer > 1.3)
					{
						animation.play("attack");
					}
					
					if (attackTimer > 1.5)
					{
						var dir:int = (FP.world as BaseWorld).player.x - x;
						dir /= Math.abs(dir);
						animation.play("stand");
						
						for (var i:int = 0; i < 3; i++)
						{
							var shurk:Shurikan = new Shurikan(x, y-22, 250 * dir, i * -80);
							FP.world.add(shurk);
							attackTimer = 0;
						}
					}
				}
				
				if (damageTime > 0)
				{
					animation.alpha = 0.5;
				}
				else
				{
					animation.alpha = 1;
				}
			}
			else
			{
				deathTimer += FP.elapsed;
				animation.alpha = FP.lerp(1, 0, deathTimer / 1);
				if (deathTimer > 1)
				{
					FP.world.remove(this);
				}
			}
			
			animation.flipped = (FP.world as BaseWorld).player.x < x ? false : true;
			
			yVel += 450 * FP.elapsed;
			
			if (collide("ground", x + xVel * FP.elapsed, y))
			{
				xVel = 0;
			}
			if (collide("ground", x, y - 1) && yVel < 0)
			{
				yVel = 0;
				
				while (collide("ground", x, y))
				{
					y += 1;
				}
			}
			if (collide("ground", x, y+1) && yVel >= 0)
			{
				yVel = 0;
				
				while (collide("ground", x, y))
				{
					y -= 1;
				}
			}
			
			if (hideTimer > 0)
			{
				collidable = false;
				hideTimer -= FP.elapsed;
				
				if (hideTimer < 0.7 && hideTimer + FP.elapsed > 0.7)
				{
					xVel = (Math.random() > 0.5 ? -200 : 200);
				}
				
				if (hideTimer <= 0)
				{
					animation.play("return");
					collidable = true;
				}
			}
			
			if (!collide("ground", x + xVel * FP.elapsed * 5, y + 1)) // Gonna fall off a cliff
			{
				xVel = 0;
			}
			
			if (damageTime > 0)
			{
				damageTime -= FP.elapsed;
				animation.alpha = 0.5;
			}
			else
			{
				animation.alpha = 1;
			}
			
			x += xVel * FP.elapsed;
			y += yVel * FP.elapsed;
		}
		
		override public function damage(damage:int, attacker:Entity):void
		{
			super.damage(damage, attacker);
			
			animation.play("stand");
			damageTime = 0.4;
			hitDirection = x - attacker.x;
			hitDirection = hitDirection / Math.abs(hitDirection);
			
			if (health <= 0)
			{
				collidable = false;
				setHitbox(0,-500,1,1);
				yVel = -200;
				deathTimer = 0;
				
				animation.play("stand");
			}
		}
	}

}