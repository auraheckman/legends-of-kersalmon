package game 
{
	import net.flashpunk.*;
	import net.flashpunk.utils.Draw;
	/**
	 * ...
	 * @author ...
	 */
	public class AttackerVolume extends Entity
	{
		public var damageEnemies:Boolean;
		public var damagePlayer:Boolean;
		
		public var parent:Entity;
		public var life:Number;
		
		public var hitList:Array;
		
		public function AttackerVolume(parent:Entity, life:Number) 
		{
			this.parent = parent;
			this.life = life;
			this.hitList = [];
			
			this.setHitboxTo(parent);
			
			x = parent.x;
			y = parent.y;
		}
		
		override public function update():void 
		{
			super.update();
			
			if (damageEnemies == true)
			{
				type = "playerdamage";
			}
			
			if (damageEnemies)
			{
				var collisions:Array = [];
				collideTypesInto(["enemy"], x, y, collisions);
				
				for each(var e:BaseEntity in collisions)
				{
					if (hitList.indexOf(e) > -1)
					{
						continue;
					}
					
					e.damage(10, parent);
					hitList.push(e);
				}
				
				collisions = [];
				collideTypesInto(["chest"], x, y, collisions);
				
				for each(var chest:Chest in collisions)
				{
					chest.open();
				}
			}
			
			if (damagePlayer)
			{
				var collisions:Array = [];
				collideTypesInto(["player"], x, y, collisions);
				
				for each(var e:BaseEntity in collisions)
				{
					if (hitList.indexOf(e) > -1)
					{
						continue;
					}
					
					e.damage(10, parent);
					hitList.push(e);
				}
			}
			
			life -= FP.elapsed;
			if (life <= 0)
			{
				FP.world.remove(this);
			}
		}
	}

}