package game 
{
	import flash.display.Sprite;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.Sfx;
	/**
	 * ...
	 * @author ...
	 */
	public class Activatable extends Entity 
	{
		[Embed(source = "../Assets/Switch.png")]
		private const SWITCH:Class;
		
		[Embed(source = "../Assets/Door.png")]
		private const DOOR:Class;
		
		[Embed(source = "../Assets/Breakable.png")]
		private const BREAKABLE:Class;
		
		[Embed(source = "../sfx/switch.mp3")]
		private const SWITCH_SOUND:Class;
		
		public var activates:int;
		
		public var animation:Spritemap;
		
		public function Activatable(x:int, y:int, type:String, id:int, activates:int) 
		{
			this.x = x;
			this.y = y;
			if (type == "door" || type == "breakable")
			{
				this.x -= this.x % 32;
				this.y -= this.y % 32;
				this.y += 1;
			}
			else
			{
				this.x -= this.x % 16;
				this.y -= this.y % 16;
			}
			
			name = id.toString();
			this.activates = activates;
			
			this.type = type;
			if (type == "switch")
			{
				animation = new Spritemap(SWITCH, 32, 32);
				animation.add("off", [0], 1);
				animation.add("on", [1], 1);
				addGraphic(animation);
				animation.play("off");
				animation.centerOO();
				
				setHitbox(32, 32);
			}
			else if (type == "door")
			{
				animation = new Spritemap(DOOR, 32, 96);
				animation.add("off", [0], 1);
				animation.add("on", [1], 1);
				addGraphic(animation);
				animation.play("off");
				
				setHitbox(32, 96);
			}
			else if (type == "breakable")
			{
				animation = new Spritemap(BREAKABLE, 32, 96);
				animation.add("off", [0], 1);
				animation.add("on", [1], 1);
				addGraphic(animation);
				animation.play("off");
				
				setHitbox(32, 96);
			}
		}
		
		override public function update():void 
		{
			super.update();
			
			if (type == "switch" && animation.currentAnim == "off")
			{
				if (collide("playerdamage", x, y))
				{
					var sound:Sfx = new Sfx(SWITCH_SOUND);
					sound.volume = 0.3;
					sound.play();
					
					activate();
				}
			}
			
			if (type == "switch" && animation.currentAnim == "on")
			{
				animation.angle += FP.elapsed * 360;
			}
		}
		
		public function activate():void
		{
			if (type == "switch")
			{
				var other:Activatable = FP.world.getInstance(activates.toString()) as Activatable;
				other.activate();
				animation.play("on");
			}
			if (type == "door" || type == "breakable")
			{
				animation.play("on");
				collidable = false;
			}
		}
	}

}