package game 
{
	import net.flashpunk.*;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.*;
	/**
	 * ...
	 * @author ...
	 */
	public class BaseEntity extends Entity
	{
		[Embed(source = "../sfx/enemyhit.mp3")]
		private const ENEMY_HIT:Class;
		
		[Embed(source = "../sfx/playerhit.mp3")]
		private const PLAYER_HIT:Class;
		
		public var xVel:int, yVel:int;
		public var health:int;
		
		public function BaseEntity() 
		{
			yVel = xVel = 0;
		}
		
		public function damage(damage:int, attacker:Entity):void
		{
			health -= damage;
			
			if (attacker.type == "player")
			{
				var sound:Sfx = new Sfx(PLAYER_HIT);
				sound.volume = 0.3;
				sound.play();
			}
			else
			{
				var sound:Sfx = new Sfx(ENEMY_HIT);
				sound.volume = 0.3;
				sound.play();
			}
		}
		
		override public function render():void 
		{
			super.render();
			
			if (Input.check(Key.P))
			{
				Draw.hitbox(this);
			}
		}
	}

}