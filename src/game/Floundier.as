package game 
{
	import net.flashpunk.*;
	import net.flashpunk.graphics.Anim;
	import net.flashpunk.graphics.Spritemap;
	/**
	 * ...
	 * @author ...
	 */
	public class Floundier extends BaseEntity
	{
		[Embed(source = "../Assets/Floudier.png")]
		private const FLOUNDIER:Class;
		
		private var animation:Spritemap;
		
		private var damageTime:Number;
		private var hitDirection:Number;
		
		private var attackTimer:Number;
		private var chargeTimer:Number;
		private var deathTimer:Number;
		
		private var hasShield:Boolean;
		private var foundPlayer:Boolean;
		
		public function Floundier() 
		{
			animation = new Spritemap(FLOUNDIER, 128, 64);
			animation.add("stand", [0], 10);
			animation.add("walk", [0, 1, 2], 10);
			animation.add("attack", [3, 4, 5, 6], 20, false);
			
			animation.add("standnoshield", [7], 10);
			animation.add("walknoshield", [7, 8, 9], 20);
			animation.add("attacknoshield", [10, 11, 12, 13], 20, false);
			
			animation.play("stand");
			
			animation.originX = 64;
			animation.originY = 64;
			addGraphic(animation as Graphic);
			
			setHitbox(28, 46, 14, 46);
			
			health = 36;
			
			type = "enemy";
			
			xVel = yVel = 0;
			chargeTimer = 1;
			attackTimer = 0;
			hasShield = true;
		}
		
		override public function update():void 
		{
			super.update();
			
			if (health > 0)
			{
				if (damageTime > 0)
				{
					xVel = (300 - HeckMath.easeOut(0, 200, (0.4 - damageTime) / 0.4)) * hitDirection;
					attackTimer = 0;
					chargeTimer = 0;
					damageTime -= FP.elapsed;
					if (damageTime <= 0)
					{
						animation.play((hasShield ? "stand" : "standnoshield"));
					}
				}
				else
				{
					xVel = 0;
					// AI
					var player:Player = (FP.world as BaseWorld).player;
					
					if (FP.distance(player.x, player.y, x, y) < 8*32 && chargeTimer <= 0)
					{
						attackTimer += FP.elapsed;
						
						if (attackTimer > 0.8)
						{
							animation.play((hasShield ? "attack" : "attacknoshield"), true);
						}
						
						if (attackTimer > 1)
						{
							chargeTimer = 0.4;
							hitDirection = player.x - x;
							hitDirection /= Math.abs(hitDirection);
							if (hitDirection == 0) hitDirection = 1;
							
							if (hitDirection > 0)
							{
								animation.flipped = true;
							}
							else
							{
								animation.flipped = false;
							}
						}
						
						foundPlayer = true;
					}
					
					if (chargeTimer > 0)
					{
						chargeTimer -= FP.elapsed;
						xVel = (650 - HeckMath.easeOut(0, 650, (0.4 - chargeTimer) / 0.4)) * hitDirection;
						
						var vol:AttackerVolume = new AttackerVolume(this, 0.02);
						vol.damagePlayer = true;
						vol.width *= 1.5;
						vol.x += (animation.flipped ? 35 : -35);
						FP.world.add(vol);
						
						if (chargeTimer <= 0)
						{
							attackTimer = 0;
							animation.play((hasShield ? "stand" : "standnoshield"));
						}
					}
					else
					{
						if (foundPlayer && Math.abs(player.x - x) > 20)
						{
							xVel = player.x - x;
							xVel /= Math.abs(xVel);
							xVel *= (hasShield ? 70 : 130);
							animation.play((hasShield ? "walk" : "walknoshield"));
							
							if (xVel > 0) animation.flipped = true;
							else animation.flipped = false;
						}
					}
				}
				
				if (damageTime > 0 && !hasShield)
				{
					animation.alpha = 0.5;
				}
				else
				{
					animation.alpha = 1;
				}
			}
			else
			{
				deathTimer += FP.elapsed;
				animation.alpha = FP.lerp(1, 0, deathTimer / 1);
				if (deathTimer > 1)
				{
					FP.world.remove(this);
				}
			}
			
			yVel += 450 * FP.elapsed;
			
			if (collide("ground", x + xVel * FP.elapsed, y))
			{
				xVel = 0;
			}
			if (collide("ground", x, y - 1) && yVel < 0)
			{
				yVel = 0;
				
				while (collide("ground", x, y))
				{
					y += 1;
				}
			}
			if (collide("ground", x, y+1) && yVel >= 0)
			{
				yVel = 0;
				
				while (collide("ground", x, y))
				{
					y -= 1;
				}
			}
			
			if (!collide("ground", x + xVel * FP.elapsed * 10, y + 1)) // Gonna fall off a cliff
			{
				xVel = 0;
			}
			
			x += xVel * FP.elapsed;
			y += yVel * FP.elapsed;
		}
		
		
		override public function damage(damage:int, attacker:Entity):void
		{
			if (hasShield)
			{
				if (damage > 90)
				{
					animation.play("standnoshield");
					damageTime = 0.4;
					hitDirection = x - attacker.x;
					hitDirection = hitDirection / Math.abs(hitDirection);
					attackTimer = 0;
					hasShield = false;
				}
				else
				{
					animation.play("walk");
					damageTime = 0.4;
					hitDirection = x - attacker.x;
					hitDirection = hitDirection / Math.abs(hitDirection);
					hitDirection /= 2;
				}
			}
			else
			{
				super.damage(damage, attacker);
				
				animation.play("walknoshield");
				damageTime = 0.4;
				hitDirection = x - attacker.x;
				hitDirection = hitDirection / Math.abs(hitDirection);
				
				if (health <= 0)
				{
					collidable = false;
					setHitbox(0,-500,1,1);
					yVel = -200;
					deathTimer = 0;
					
					animation.play("standnoshield");
				}
			}
		}
	}

}