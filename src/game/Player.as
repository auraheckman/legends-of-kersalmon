package game 
{	
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Anim;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.Sfx;
	import net.flashpunk.utils.Draw;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.FP;
	import net.flashpunk.World;
	/**
	 * ...
	 * @author ...
	 */
	public class Player extends BaseEntity
	{
		[Embed(source = "../sfx/enemyhit.mp3")]
		private const SOUND_HIT:Class;
		
		[Embed(source = "../sfx/fireball.mp3")]
		private const SOUND_FIREBALL:Class;
		
		[Embed(source = "../sfx/watergun.mp3")]
		private const SOUND_WATERGUN:Class;
		
		[Embed(source="../sfx/land.mp3")]
		private const SOUND_LAND:Class;
		
		[Embed(source="../sfx/playerswing.mp3")]
		private const SOUND_SWING:Class;
		
		[Embed(source = "../Assets/MainCharKoi.png")] 
		private const PLAYER:Class;
		
		[Embed(source = "../Assets/MainCharArcher.png")]
		private const PLAYER_ARCHER:Class;
		
		[Embed(source = "../Assets/MainCharSnapper.png")]
		private const PLAYER_SNAPPER:Class;
		
		[Embed(source = "../Assets/MainCharPuffer.png")]
		private const PLAYER_PUFFER:Class;
		
		public var animation:Spritemap;
		public var animationKoi:Spritemap;
		public var animationArcher:Spritemap;
		public var animationSnapper:Spritemap;
		public var animationPuffer:Spritemap;
		
		public var hangTime:Number;
		public var attackTime:Number;
		public var hitTimer:Number;
		
		public var isFishing:Boolean;
		public var fishWidth:int;
		public var fishX:Number;
		public var fishVelX:int;
		public var fishCompletion:Number;
		public var fishFailure:Number;
		public var baitX:Number;
		public var baitVel:int;
		
		public var deathTimer:Number;
		
		private var canUsePuffer:Boolean;
		
		public function Player() 
		{
			animationKoi = new Spritemap(PLAYER, 128, 64);
			
			animationKoi.add("stand", [0], 5, true);
			animationKoi.add("jumpup", [9], 5, true);
			animationKoi.add("jumpdown", [10, 11], 10, false);
			animationKoi.add("walk", [1, 2, 3, 4, 5, 6, 7, 8], 10, true);
			animationKoi.add("attack", [12, 12, 12, 13, 14, 15], 20, false);
			animationKoi.add("special", [0, 16, 17, 18, 18, 18], 20, false);
			animationKoi.play("walk");
			animationKoi.originX = 64;
			animationKoi.originY = 64;
			addGraphic(animationKoi as Graphic);
			animation = animationKoi;
			
			animationArcher = new Spritemap(PLAYER_ARCHER, 128, 64);
			
			animationArcher.add("stand", [0], 5, true);
			animationArcher.add("jumpup", [9], 5, true);
			animationArcher.add("jumpdown", [10, 11], 10, false);
			animationArcher.add("walk", [1, 2, 3, 4, 5, 6, 7, 8], 10, true);
			animationArcher.add("attack", [12, 12, 12, 13, 14, 15], 20, false);
			animationArcher.add("special", [0, 16, 17, 18, 18, 18], 20, false);
			animationArcher.play("walk");
			animationArcher.originX = 64;
			animationArcher.originY = 64;
			
			animationSnapper = new Spritemap(PLAYER_SNAPPER, 128, 64);
			
			animationSnapper.add("stand", [0], 5, true);
			animationSnapper.add("jumpup", [9], 5, true);
			animationSnapper.add("jumpdown", [10, 11], 10, false);
			animationSnapper.add("walk", [1, 2, 3, 4, 5, 6, 7, 8], 10, true);
			animationSnapper.add("attack", [12, 12, 12, 13, 14, 15], 20, false);
			animationSnapper.add("special", [0, 16, 17, 18, 18, 18], 20, false);
			animationSnapper.play("walk");
			animationSnapper.originX = 64;
			animationSnapper.originY = 64;
			
			animationPuffer = new Spritemap(PLAYER_PUFFER, 128, 64);
			
			animationPuffer.add("stand", [0], 5, true);
			animationPuffer.add("jumpup", [9], 5, true);
			animationPuffer.add("jumpdown", [10, 11], 10, false);
			animationPuffer.add("walk", [1, 2, 3, 4, 5, 6, 7, 8], 10, true);
			animationPuffer.add("attack", [12, 12, 12, 13, 14, 15], 20, false);
			animationPuffer.add("special", [0, 16, 17, 18, 18, 18], 20, false);
			animationPuffer.play("walk");
			animationPuffer.originX = 64;
			animationPuffer.originY = 64;
			
			setHitbox(20, 36, 10, 36);
			
			attackTime = 0;
			type = "player";
			hitTimer = 0;
			fishCompletion = 0;
			
			health = 100;
			
			deathTimer = 0;
		}
		
		override public function  update():void 
		{
			super.update();
			
			if (Stats.GetInstance().didWin)
			{
				return;
			}
			
			if (deathTimer > 0)
			{
				deathTimer -= FP.elapsed;
				animation.alpha = FP.lerp(1, 0, 1 - deathTimer);
				
				if (deathTimer <= 0)
				{
					FP.world = new World1(Stats.GetInstance().safetyZone, 160, 32 * 11);
					Stats.GetInstance().health = Stats.GetInstance().maxHealth;
				}
				
				return;
			}
			
			// Fishing
			if (FP.world.typeCount("dialog") == 0)
			{
				if (isFishing)
				{
					if (Input.check(Key.LEFT))
					{
						baitVel = -20;
					}
					else if (Input.check(Key.RIGHT))
					{
						baitVel = 20;
					}
					else
					{
						baitVel = 0;
					}
					
					if (FP.random > 0.95)
					{
						fishVelX = (0.5 + (FP.random/2) * 10) * (FP.random > 0.5 ? -1 : 1);
					}
					else if (Math.random() > 0.9)
					{
						fishX += (0.5 + (FP.random/2) * 5) * (FP.random > 0.5 ? -1 : 1);
					}
					
					baitX += baitVel * FP.elapsed;
					fishX += fishVelX * FP.elapsed;
					
					if (baitX > 39) baitX = 39;
					if (baitX < 0) baitX = 0;
					
					if (fishX > 40 - fishWidth / 2) fishX = 40 - fishWidth / 2;
					if (fishX < 0 + fishWidth / 2) fishX = 0 + fishWidth / 2;
					
					if (baitX > fishX - fishWidth / 2 && baitX < fishX + fishWidth / 2)
					{
						fishCompletion += FP.elapsed * 10;
					}
					else
					{
						fishCompletion -= FP.elapsed * 20;
					}
					
					if (fishCompletion >= 100)
					{
						var d:Dialog;
						if ((FP.world as BaseWorld).currentName == "save1") 
						{
							Stats.GetInstance().hasArcher = true;
							d = new Dialog("YOU CAUGHT AN ARCHER FISH!", null);
							d.next = new Dialog("The ARCHER FISH can shoot a water stream at jerks, it even goes through walls!  [Press R to switch your fish]", null);
						}
						if ((FP.world as BaseWorld).currentName == "save2") 
						{
							Stats.GetInstance().hasSnapper = true;
							d = new Dialog("YOU CAUGHT A RED SNAPPER!", null);
							d.next = new Dialog("The RED SNAPPER can drop time bombs that heavily damage enemies!  They also blow up weak walls, which is good too I guess.", null);
						}
						if ((FP.world as BaseWorld).currentName == "save3") 
						{
							Stats.GetInstance().hasPuffer = true;
							d = new Dialog("YOU CAUGHT A PUFFERFISH!", null);
							d.next = new Dialog("The PUFFERFISH can unleash a gust of wind that will let you jump higher.  That's cool I guess.", null);
						}
						
						if (d != null) FP.world.add(d);
						
						isFishing = false;
					}
					if (fishCompletion < 0)
					{
						isFishing = false;
					}
					
					return;
				}
				else
				{
					if ((FP.world as BaseWorld).currentName.indexOf("save") != -1)
					{
						var levelName = (FP.world as BaseWorld).currentName;
						
						fishWidth = 8;
						if (levelName == "save1") fishWidth = 14;
						if (levelName == "save2") fishWidth = 11;
						if (levelName == "save3") fishWidth = 8;
						
						var needsFish = false;
						if (levelName == "save1" && !Stats.GetInstance().hasArcher) needsFish = true;
						if (levelName == "save2" && !Stats.GetInstance().hasSnapper) needsFish = true;
						if (levelName == "save3" && !Stats.GetInstance().hasPuffer) needsFish = true;
						
						if (needsFish && yVel == 0 && x > 4*32 && x < 9*32 && Input.pressed(Key.F))
						{
							isFishing = true;
							fishX = 20;
							baitX = 20;
							fishVelX = 0;
							baitVel = 0;
							fishCompletion = 1;
							animation.play("stand");
							return;
						}
					}
				}
			}
			
			// Real Game
			if (hitTimer <= 0)
			{
				if (Input.check(Key.LEFT))
				{
					animation.flipped = true;
					if (attackTime <= 0)
					{
						if (yVel == 0) animation.play("walk");
						xVel = -140;
					}
				}
				else if (Input.check(Key.RIGHT))
				{
					animation.flipped = false;
					if (attackTime <= 0)
					{
						if (yVel == 0) animation.play("walk");
						xVel = 140;
					}
				}
				else 
				{
					xVel = 0;
					if (yVel == 0 && attackTime <= 0) animation.play("stand");
				}
			}
			
			// Apply Gravity
			yVel += 450 * FP.elapsed;
			
			// Level Collisions
			if (collide("ground", x + xVel * FP.elapsed, y))
			{
				xVel = 0;
			}
			if (collide("door", x + xVel * FP.elapsed, y))
			{
				xVel = 0;
			}
			if (collide("breakable", x + xVel * FP.elapsed, y))
			{
				xVel = 0;
			}
			if (collide("ground", x, y - 1) && yVel < 0)
			{
				yVel = 0;
				
				while (collide("ground", x, y))
				{
					y += 1;
				}
				
				hangTime = 0.4;
			}
			if (collide("ground", x, y+1) && yVel >= 0)
			{
				if (yVel > 100)
				{
					var swingsfx:Sfx = new Sfx(SOUND_LAND);
					swingsfx.volume = 0.3;
					swingsfx.play();
				}
				
				canUsePuffer = true;
				yVel = 0;
					
				while (collide("ground", x, y))
				{
					y -= 1;
				}
				
				hangTime = 0;
			}
			
			// Jumping
			if (Input.check(Key.SPACE) && hangTime < 0.4 && yVel <= 0)
			{
				yVel = HeckMath.easeOut( -300, 80, hangTime / 0.4);
				hangTime += FP.elapsed;
			}
			if (Input.released(Key.SPACE))
			{
				hangTime = 1;
				if (yVel < 0)
				{
					yVel *= 0.5;
				}
			}
			
			if (attackTime <= 0)
			{
				if (yVel > 0)
				{
					animation.play("jumpdown");
				}
				else if (yVel < 0)
				{
					animation.play("jumpup");
				}
			}
			
			attackTime -= FP.elapsed;
			if (Input.pressed(Key.F) && attackTime <= 0)
			{
				attackTime = 0.4;
				if (Input.check(Key.UP))
				{
					animation.play("special", true);
					specialAttack();
				}
				else
				{
					animation.play("attack", true);
					
					var swingsfx:Sfx = new Sfx(SOUND_SWING);
					swingsfx.volume = 0.3;
					swingsfx.play();
					
					var attackVol:AttackerVolume = new AttackerVolume(this, 0.3);
					attackVol.setHitbox(50, 48, 25, 48);
					attackVol.x += (15 * (animation.flipped ? -1 : 1));
					attackVol.x += xVel * FP.elapsed * 3;
					attackVol.damageEnemies = true;
					FP.world.add(attackVol);
				}
			}
			
			if (Input.pressed(Key.R))
			{
				swapTo();
			}
			
			if (attackTime > 0)
			{
				xVel *= 0.9;
			}
			
			x += xVel * FP.elapsed;
			y += yVel * FP.elapsed;
			
			if (x > (FP.world as BaseWorld).worldWidth - 10)
			{
				x = (FP.world as BaseWorld).worldWidth - 10
			}
			if (x < 10)
			{
				x = 10;
			}
			
			if (hitTimer > 0)
			{
				hitTimer -= FP.elapsed;
				animation.alpha = 0.5;
			}
			else
			{
				animation.alpha = 1;
			}
			
			if (FP.world.typeCount("dialog") == 0)
			{
				var teleport:Teleport = (collide("teleport", x, y) as Teleport);
				if (teleport != null)
				{
					FP.world = new World1(teleport.to, teleport.toX, teleport.toY);
				}
			}
		}
		
		public function specialAttack():void
		{
			if (animation == animationKoi)
			{
				var fb:Fireball = new Fireball((animation.flipped ? -1 : 1));
				fb.x = x;
				fb.y = y - 30;
				FP.world.add(fb);
				
				var swingsfx:Sfx = new Sfx(SOUND_FIREBALL);
				swingsfx.volume = 0.3;
				swingsfx.play();
			}
			else if (animation == animationArcher)
			{
				for (var i:int = 1; i < 5; i++)
				{
					var bull:WaterGunBullet = new WaterGunBullet(x + (animation.flipped ? -6 : 6), y - 17, (animation.flipped ? -1 : 1), -100+(i*-80));
					FP.world.add(bull);
				}
				
				var swingsfx:Sfx = new Sfx(SOUND_WATERGUN);
				swingsfx.volume = 0.3;
				swingsfx.play();
			}
			else if (animation == animationSnapper)
			{
				FP.world.add(new Bomb(x, y));
			}
			else if (animation == animationPuffer && canUsePuffer)
			{
				yVel = -300;
				canUsePuffer = false;
			}
		}
		
		override public function damage(damage:int, attacker:Entity):void 
		{
			if (hitTimer <= 0)
			{
				super.damage(damage/4, attacker);
				
				hitTimer = 0.6;
				
				var hitDirection:int = x - attacker.x;
				hitDirection /= Math.abs(hitDirection);
				xVel = hitDirection * 120;
				yVel = -200;
				
				Stats.GetInstance().health -= damage;
				
				if (Stats.GetInstance().health <= 0)
				{
					deathTimer = 1;
				}
			}
		}
		
		public function swapTo():void
		{
			var anim:String = animation.currentAnim;
			var ind:int = animation.index;
			
			if (animation == animationPuffer)
			{
				animationKoi.flipped = animation.flipped;
				animation = animationKoi;
				Stats.GetInstance().currentFish = "koi";
			}
			else if (animation == animationKoi)
			{
				animationArcher.flipped = animation.flipped;
				animation = animationArcher;
				Stats.GetInstance().currentFish = "archer";
				if (!Stats.GetInstance().hasArcher)
				{
					swapTo();
				}
			}
			else if (animation == animationArcher)
			{
				animationSnapper.flipped = animation.flipped;
				animation = animationSnapper;
				Stats.GetInstance().currentFish = "snapper";
				if (!Stats.GetInstance().hasSnapper)
				{
					swapTo();
				}
			}
			else if (animation == animationSnapper)
			{
				animationPuffer.flipped = animation.flipped;
				animation = animationPuffer;
				Stats.GetInstance().currentFish = "puffer";
				if (!Stats.GetInstance().hasPuffer)
				{
					swapTo();
				}
			}
			
			animation.play(anim, false, ind);
			
			(graphic as Graphiclist).removeAll();
			addGraphic(animation);
		}
		
		public function swapToSpecific(fish:String):void
		{
			var anim:String = animation.currentAnim;
			var ind:int = animation.index;
			
			if (fish == "koi")
			{
				animationKoi.flipped = animation.flipped;
				animation = animationKoi;
				Stats.GetInstance().currentFish = "koi";
			}
			
			if (fish == "archer" && Stats.GetInstance().hasArcher)
			{
				animationArcher.flipped = animation.flipped;
				animation = animationArcher;
				Stats.GetInstance().currentFish = "archer";
			}
			
			if (fish == "snapper" && Stats.GetInstance().hasSnapper)
			{
				animationSnapper.flipped = animation.flipped;
				animation = animationSnapper;
				Stats.GetInstance().currentFish = "snapper";
			}
			
			if (fish == "puffer" && Stats.GetInstance().hasPuffer)
			{
				animationPuffer.flipped = animation.flipped;
				animation = animationPuffer;
				Stats.GetInstance().currentFish = "puffer";
			}
			
			(graphic as Graphiclist).removeAll();
			addGraphic(animation);
		}
		
		override public function render():void 
		{
			super.render();
			
			if (isFishing)
			{
				Draw.rect(x-6, y - 72, 27, 17, 0xFFFFFF, 1, false);
				Draw.rect(x - 20, y - 55, 40, 6, 0xFF0000, 1, false);
				Draw.rect((x - 20) + fishX-fishWidth/2, y - 55, fishWidth, 6, 0xFFFF00, 1, false);
				Draw.rectPlus(x - 20, y - 55, 40, 6, 0xFFFFFF, 1, false);
				Draw.rect((x - 20) + baitX, y - 57, 1, 10, 0x00FFFF, 1, false);
				Draw.text(FP.clamp(Math.round(fishCompletion), 0, 99) + "%", x-5, y - 73, { color:0x00FF00 } );
			}
		}
	}

}