package game 
{
	import net.flashpunk.Entity;
	/**
	 * ...
	 * @author ...
	 */
	public class Teleport extends Entity
	{
		public var to:String;
		public var toX:int;
		public var toY:int;
		
		public function Teleport(x:int, y:int, width:int, height:int, to:String, toCoords:String) 
		{
			this.to = to;
			this.toX = toCoords.split(',')[0] * 32;
			this.toY = toCoords.split(',')[1] * 32;
			
			setHitbox(width, height);
			this.x = x;
			this.y = y;
			
			type = "teleport";
		}
	}

}