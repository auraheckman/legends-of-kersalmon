package game 
{
	import flash.utils.ByteArray;
	import net.flashpunk.*;
	
	/**
	 * ...
	 * @author ...
	 */
	public class BaseWorld extends World
	{
		public var player:Player;
		public var worldWidth:int;
		public var worldHeight:int;
		public var currentName:String;
		
		public function BaseWorld() 
		{
			
		}
		
		public function loadLevel(source:*, tileSource:*):void
		{
			var rawData:ByteArray = new source(); // The embedded TMX asset
			var dataString:String = rawData.readUTFBytes(rawData.length);
			var xmlData:XML = new XML(dataString);

			var cols:uint = xmlData.@width;
			var rows:uint = xmlData.@height;
			var tileW:uint = xmlData.@tilewidth;
			var tileH:uint = xmlData.@tileheight;
			
			worldWidth = cols * tileW;
			worldHeight = cols * tileH;

			var background:String = xmlData.layer.(@name == "Background").data;
			var game:String = xmlData.layer.(@name == "Game").data;
			var details:String = xmlData.layer.(@name == "Details").data;
			var teleports:XMLList = xmlData.objectgroup.(@name == "Teleport");
			var npcs:XMLList = xmlData.objectgroup.(@name == "NPCs");
			var enemies:XMLList = xmlData.objectgroup.(@name == "Enemy");
			var chests:XMLList = xmlData.objectgroup.(@name == "Chest");
			var activates:XMLList = xmlData.objectgroup.(@name == "Activatable");
			
			var tiles:TileSet = new TileSet(tileSource, tileW * cols, tileH * rows, tileW, tileH);
			var backgroundTiles:TileSet = new TileSet(tileSource, tileW * cols, tileH * rows, tileW, tileH);
			var detailTiles:TileSet = new TileSet(tileSource, tileW * cols, tileH * rows, tileW, tileH);
			var collGrid:WorldCollision = new WorldCollision(tileW * cols, tileH * rows, tileW, tileH);
			
			var gameData:Array = (game as String).split(',');
			var bgData:Array = (background as String).split(',');
			var detailData:Array = (details as String).split(',');
			for (var x:int = 0; x < cols; x++)
			{
				for (var y:int = 0; y < rows; y++)
				{
					var val:int = bgData[x + y * cols] - 1;
					backgroundTiles.setTile(x, y, val);
					
					val = detailData[x + y * cols] - 1;
					detailTiles.setTile(x, y, val);
					
					val = gameData[x + y * cols] - 1;
					tiles.setTile(x, y, val);
					if (val >= 0)
					{
						collGrid.setTile(x, y, true);
					}
				}
			}
			
			tiles.layer = 0;
			detailTiles.layer = -100;
			backgroundTiles.layer = 100;
			
			add(tiles);
			add(detailTiles);
			add(backgroundTiles);
			add(collGrid);
			
			for each(var o:XML in teleports.object)
			{
				var tx:int = o.@x.toXMLString();
				var ty:int = o.@y.toXMLString();
				var tw:int = o.@width.toXMLString();
				var th:int = o.@height.toXMLString();
				var toMap:String = o.properties.property.(@name == "To").@value;
				var toCoords:String = o.properties.property.(@name == "Coords").@value;
				
				var tp:Teleport = new Teleport(tx, ty, tw, th, toMap, toCoords);
				add(tp);
			}
			
			for each(var n:XML in npcs.object)
			{
				var nx:int = n.@x.toXMLString();
				var ny:int = n.@y.toXMLString();
				
				var npc = new NPC(nx, ny);
				this.add(npc);
			}
			
			for each(var e:XML in enemies.object)
			{
				var ex:int = e.@x.toXMLString();
				var ey:int = e.@y.toXMLString();
				var type:String = e.@type.toXMLString();
				
				var enemy:BaseEntity = null; 
				if (type == "Squid")
				{
					enemy = new SquidKnight();
					enemy.x = ex;
					enemy.y = ey;
				}
				else if (type == "Octopus")
				{
					enemy = new Octoninja();
					enemy.x = ex;
					enemy.y = ey;
				}
				else if (type == "Flounder")
				{
					enemy = new Floundier();
					enemy.x = ex;
					enemy.y = ey;
				}
				else if (type == "Piranha")
				{
					enemy = new Piranha();
					enemy.x = ex;
					enemy.y = ey;
				}
				if (enemy != null)
				{
					this.add(enemy);
				}
			}
			
			for each(var o:XML in chests.object)
			{
				var tx:int = o.@x.toXMLString();
				var ty:int = o.@y.toXMLString();
				var tid:String = o.@id.toXMLString();
				
				var chest:Chest = new Chest(tx, ty, tid);
				add(chest);
			}
			
			for each(var o:XML in activates.object)
			{
				var tx:int = o.@x.toXMLString();
				var ty:int = o.@y.toXMLString();
				var id:int = o.@id.toXMLString();
				var activatesID:int = o.properties.property.(@name == "Activates").@value;
				var type:String = o.@type.toXMLString();
				
				var active:Activatable = new Activatable(tx, ty, type, id, activatesID)
				add(active);
			}
		}
	}

}