package game 
{
	import flash.display.Sprite;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Spritemap;
	/**
	 * ...
	 * @author ...
	 */
	public class Fireball extends Entity 
	{
		[Embed(source = "../Assets/Fireball.png")]
		private const FIREBALL:Class;
		
		private var animation:Spritemap;
		private var xVel:int, yVel:int;
		private var life:Number;
		
		public function Fireball(direction:int) 
		{
			animation = new Spritemap(FIREBALL, 16, 16);
			animation.add("play", [0, 1, 2], 20, true);
			animation.play("play");
			addGraphic(animation);
			
			animation.centerOO();
			setHitbox(8, 8, 4, 4);
			
			yVel = 0;
			xVel = direction * 220;
			
			type = "playerdamage";
		}
		
		override public function update():void 
		{
			super.update();
			
			animation.angle += FP.elapsed * 720;
			
			x += xVel * FP.elapsed;
			y += yVel * FP.elapsed;
			
			var enemy:BaseEntity = collide("enemy", x, y) as BaseEntity;
			if (enemy != null)
			{
				enemy.damage(20, this);
				FP.world.remove(this);
			}
			
			if (collide("ground", x, y))
			{
				FP.world.remove(this);
			}
			
			life += FP.elapsed;
			if (life > 4)
			{
				FP.world.remove(this);
			}
		}
	}

}