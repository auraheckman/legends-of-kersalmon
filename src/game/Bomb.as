package game 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.Sfx;
	import net.flashpunk.utils.Draw;
	/**
	 * ...
	 * @author ...
	 */
	public class Bomb extends Entity
	{
		[Embed(source = "../sfx/explode.mp3")]
		private const SOUND_EXPLODE:Class;
		
		[Embed(source = "../Assets/Bomb.png")]
		private const BOMB:Class;
		
		public var animation:Spritemap;
		private var timer:Number;
		private var hitList:Array;
		
		private var xVel:int, yVel:int;
		
		public function Bomb(x:int, y:int) 
		{
			this.x = x;
			this.y = y;
			
			animation = new Spritemap(BOMB, 128, 64);
			animation.add("count", [0, 1, 2], 2, false);
			animation.add("explode", [3, 4, 5, 6, 7, 8], 30, false);
			animation.originX = animation.originY = 64;
			animation.originY = 52;
			animation.play("count");
			addGraphic(animation);
			
			centerOrigin();
			
			setHitbox(20, 20, 10, 10);
			
			timer = 0;
			hitList = [];
		}
		
		override public function update():void 
		{
			super.update();
			
			timer += FP.elapsed;
			if (timer > 1.5)
			{
				if (timer - FP.elapsed <= 1.5)
				{
					var sound:Sfx = new Sfx(SOUND_EXPLODE);
					sound.volume = 0.3;
					sound.play();
				}
				
				setHitbox(128, 64, 64, 64);
				animation.play("explode");
				
				var collisions:Array = [];
				collideTypesInto(["enemy"], x, y, collisions);
				
				for each(var e:BaseEntity in collisions)
				{
					if (hitList.indexOf(e) > -1)
					{
						continue;
					}
					
					e.damage(100, this);
					hitList.push(e);
				}
				
				var breakable:Activatable = collide("breakable", x, y) as Activatable;
				if (breakable != null)
				{
					breakable.activate();
				}
				
				var orb:Orb = collide("orb", x, y) as Orb;
				if (orb != null)
				{
					orb.activate();
				}
			}
			else
			{
				yVel += 300 * FP.elapsed;
			
				if (collide("ground", x, y+1) && yVel >= 0)
				{
					yVel = 0;
					
					while (collide("ground", x, y))
					{
						y -= 1;
					}
				}
				
				x += xVel * FP.elapsed;
				y += yVel * FP.elapsed;
			}
			
			if (timer > 1.7)
			{
				FP.world.remove(this);
			}
		}
	}

}