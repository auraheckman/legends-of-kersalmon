package game 
{
	import net.flashpunk.*;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Tilemap;
	/**
	 * ...
	 * @author ...
	 */
	public class TileSet extends Entity
	{
		public var tiles:Tilemap;
		
		public function TileSet(source:*, height:int, width:int, tileW:int, tileH:int) 
		{
			x = y = 0;
			tiles = new Tilemap(source, height, width, tileW, tileH);
			addGraphic(tiles);
		}
		
		public function setTile(x:int, y:int, id:int):void
		{
			tiles.setTile(x, y, id);
		}
	}

}