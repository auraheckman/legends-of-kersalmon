package game 
{
	import net.flashpunk.*;
	import net.flashpunk.graphics.Spritemap;
	/**
	 * ...
	 * @author ...
	 */
	public class Orb extends Entity
	{
		[Embed(source = "../Assets/Orb.png")]
		private const ORB:Class;
		
		private var animation:Spritemap;
		private var showingBomb:Boolean;
		private var showTimer:Number;
		private var failCounter:int;
		
		[Embed(source = "../sfx/switch.mp3")]
		private const SOUND_SWITCH:Class;
		
		[Embed(source = "../sfx/yell.mp3")]
		private const SOUND_YELL:Class;
		
		private var winTimer:Number;
		
		public function Orb() 
		{
			animation = new Spritemap(ORB, 64, 96);
			animation.add("wait", [0]);
			animation.add("fish", [0, 1, 2, 3, 4], 10, false);
			animation.add("bomb", [1, 5, 6, 7, 8], 10, false);
			animation.add("win", [0, 0, 0, 1, 0, 1, 0, 1, 0, 2, 0, 2, 0, 2, 0, 0, 0, 9, 10, 11, 12], 10, false);
			animation.play("wait");
			addGraphic(animation);
			animation.originX = 32;
			animation.originY = 96;
			
			originX = 32;
			originY = 96;
			
			showTimer = 0;
			layer = 1;
			type = "orb";
			
			winTimer = 0;
			
			setHitbox(64, 96, 32, 96);
		}
		
		override public function update():void 
		{
			super.update();
			
			if (Stats.GetInstance().didWin)
			{
				if (winTimer == 0)
				{
					var sound:Sfx = new Sfx(SOUND_YELL);
					sound.volume = 0.5;
					sound.play();
				}
				
				winTimer += FP.elapsed;
				if (winTimer > 1 && winTimer - FP.elapsed <= 1)
				{
					var sound:Sfx = new Sfx(SOUND_SWITCH);
					sound.volume = 0.5;
					sound.play();
				}
			}
			
			showTimer += FP.elapsed;
			if (showTimer > 8 && !Stats.GetInstance().didWin)
			{
				if (FP.random > 0.75 || failCounter >= 3)
				{
					animation.play("bomb", true);
					showingBomb = true;
					failCounter = 0;
				}
				else
				{
					animation.play("fish", true);
					showingBomb = false;
					failCounter += 1;
				}
				showTimer = 0;
			}
			
			var boss:Boss = FP.world.getInstance("Boss");
			if (boss != null && boss.health <= 0 && !Stats.GetInstance().didWin)
			{
				Stats.GetInstance().didWin = true;
				animation.play("win");
				boss.deathTimer = 3;
				boss.mortalize();
				Stats.GetInstance().music.stop();
			}
		}
		
		public function activate():void
		{
			if (showingBomb == true)
			{
				var boss:Boss = FP.world.getInstance("Boss");
				boss.mortalize();
				showingBomb = false;
				animation.play("fish");
				showTimer = 0;
			}
		}
	}

}