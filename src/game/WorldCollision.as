package game 
{
	import flash.display.Sprite;
	import flash.utils.ByteArray;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Tilemap;
	import net.flashpunk.masks.Grid;
	import net.flashpunk.World;
	/**
	 * ...
	 * @author ...
	 */
	public class WorldCollision extends Entity
	{
		[Embed(source = "../levels/Test.tmx", mimeType = "application/octet-stream")]
		private const LEVEL_DATA:Class;
		
		[Embed(source = "../Assets/ForestTiles.png")]
		private const LEVEL_TILES:Class;
		
		public var collGrid:Grid;
		
		public function WorldCollision(width:int, height:int, tileW:int, tileH:int) 
		{
			collGrid = new Grid(width, height, tileW, tileH);
			mask = collGrid;
			type = "ground";
		}
		
		public function setTile(x:int, y:int, solid:Boolean):void
		{
			collGrid.setTile(x, y, solid);
		}
	}

}