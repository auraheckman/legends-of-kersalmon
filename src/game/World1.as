package game 
{
	import flash.utils.ByteArray;
	import net.flashpunk.*;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Tilemap;
	import net.flashpunk.masks.Grid;
	
	/**
	 * ...
	 * @author ...
	 */
	public class World1 extends BaseWorld
	{	
		[Embed(source = "../Assets/MagicFish.png")]
		private const MAGICFISH:Class;
		
		[Embed(source = "../Assets/BG.png")]
		private const BACKGROUND:Class;
		
		[Embed(source = "../Assets/BGTree.png")]
		private const SAVE_BACKGROUND:Class;
		
		[Embed(source = "../Assets/BGCastle.png")]
		private const CASTLE_BACKGROUND:Class;
		
		[Embed(source = "../Assets/ForestTiles.png")]
		private const FOREST_TILES:Class;
		
		[Embed(source = "../Assets/CastleTiles.png")]
		private const CASTLE_TILES:Class;
		
		private var ui:UI;
		
		public function World1(name:String, x:int, y:int) 
		{
			player = new Player();
			player.x = x;
			player.y = y;
			player.swapToSpecific(Stats.GetInstance().currentFish);
			add(player);
			
			loadLevel(WorldSelector.levelByName(name), (name.indexOf("castle") != -1 ? CASTLE_TILES : FOREST_TILES));
			
			currentName = name;
			if (name.indexOf("save") != -1) // is a save spot.
			{
				add(new Background(SAVE_BACKGROUND, false));
				Stats.GetInstance().playMusic("Save");
				Stats.GetInstance().health = Stats.GetInstance().maxHealth;
				Stats.GetInstance().safetyZone = name;
			}
			else if (name.indexOf("castle") != -1) // is Castle
			{
				add(new Background(CASTLE_BACKGROUND, true));
				if (name.indexOf("boss") != -1)
				{
					Stats.GetInstance().playMusic("Boss");
				}
				else
				{
					Stats.GetInstance().playMusic("Castle");
				}
			}
			else
			{
				add(new Background(BACKGROUND, false));
				Stats.GetInstance().playMusic("Forest");
			}
			
			if (name == "castleboss")
			{
				var boss:Boss = new Boss();
				boss.x = 32 * 11;
				boss.y = 320 - 32;
				add(boss);
				
				var orb:Orb = new Orb();
				orb.x = (32 * 13)/2;
				orb.y = 320 - 32;
				add(orb);
			}
			
			if (name == "home" && !Stats.GetInstance().didintro)
			{
				var fishanim:Spritemap = new Spritemap(MAGICFISH, 64, 64);
				fishanim.add("play", [0, 1], 5, true);
				fishanim.play("play");
				
				var magicfish:Entity = new Entity(20, 32 * 8, fishanim);
				add(magicfish);
			}
			
			ui = new UI();
			add(ui);
		}
		
		override public function update():void 
		{
			super.update();
			
			ui.fishUI.play(Stats.GetInstance().currentFish);
			
			camera.x = player.x;
			camera.y = player.y;
			camera.offset( -200, -200);
			
			if (camera.x < 0) camera.x = 0;
			if (camera.x > worldWidth - 400) camera.x = worldWidth - 400;
			if (camera.y > worldHeight - 400) camera.y = worldHeight - 400;
			
			if (currentName == "home")
			{
				camera.y = 10;
			}
			
			if (currentName == "home" && !Stats.GetInstance().didintro)
			{
				Stats.GetInstance().didintro = true;
				
				var d:Dialog = new Dialog("Hey kid...", null);
				this.add(d);
				
				d.next = new Dialog("I need your help.  My fish wife, fairest fish in all of Kersalmon, has been captured by the evil Fisher King.", null);
				
				d.next.next = new Dialog("Alas, I am without legs.  So I can not pursue him.  If you would kindly go rough him up and get my fish wife back, I would be in you debt.", null);
				
				d.next.next.next = new Dialog("If you accept, he lives in the big castle, dead East of here.  Watch out for the Fishman Warriors on the way!", null);
				
				d.next.next.next.next = new Dialog("I'll let you in on something to help you out!  If you hold [UP] while attacking, [F], you will use the fish's secret power.", null);
			}
			
			if (currentName == "save1" && !Stats.GetInstance().knowsFishing)
			{
				Stats.GetInstance().knowsFishing = true;
				
				var d:Dialog = new Dialog("Wow, it's a 'Fish Tree' close to the village.  This is the perfect place to catch a new fish to fight with!", null);
				this.add(d);
				
				d.next = new Dialog("[Go to the center dock and swing your rod to start fishing.  Try to manuever your lure to where the yellow bar is to catch the fish.]", null);
			}
			
			if (currentName == "save2" && !Stats.GetInstance().knowsSnapper)
			{
				Stats.GetInstance().knowsSnapper = true;
				
				var d:Dialog = new Dialog("I should try fishing in this pond too.  I bet there's a rad fish inside.", null);
				this.add(d);
				
				d.next = new Dialog("[Go to the center dock and swing your rod to start fishing.  Try to manuever your lure to where the yellow bar is to catch the fish.]", null);
			}
			
			if (currentName == "save3" && !Stats.GetInstance().knowsPuffer)
			{
				Stats.GetInstance().knowsPuffer = true;
				
				var d:Dialog = new Dialog("Whoa there's another pond inside the castle!  This has to be full of powerful fish.", null);
				this.add(d);
				
				d.next = new Dialog("[Go to the center dock and swing your rod to start fishing.  Try to manuever your lure to where the yellow bar is to catch the fish.]", null);
			}
		}
	}

}