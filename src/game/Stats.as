package game 
{
	import net.flashpunk.Sfx;
	import net.flashpunk.*;
	/**
	 * ...
	 * @author ...
	 */
	public class Stats 
	{
		[Embed(source = "../music/Boss.mp3")]
		private const MUSIC_BOSS:Class;
		
		[Embed(source = "../music/Castle.mp3")]
		private const MUSIC_CASTLE:Class;
		
		[Embed(source = "../music/Forest.mp3")]
		private const MUSIC_FOREST:Class;
		
		[Embed(source = "../music/Save.mp3")]
		private const MUSIC_SAVE:Class;
		
		private var currentMusic:String = "";
		public var music:Sfx;
		
		private static var instance:Stats;
		
		public var worms:int;
		
		public var didintro:Boolean;
		public var knowsFishing:Boolean;
		public var knowsSnapper:Boolean;
		public var knowsPuffer:Boolean;
		public var hasArcher:Boolean;
		public var hasSnapper:Boolean;
		public var hasPuffer:Boolean;
		
		public var currentFish:String;
		
		public var health:int;
		public var maxHealth:int;
		
		public var chests:Array;
		
		public var safetyZone:String;
		
		public var didWin:Boolean;
		
		public function Stats() 
		{
			
		}
		
		public static function GetInstance():Stats
		{
			if (instance == null)
			{
				instance = new Stats();
				instance.currentFish = "koi";
				instance.health = instance.maxHealth = 70;
				instance.chests = [];
				instance.safetyZone = "home";
				instance.didWin = false;
			}
			return instance;
		}
		
		public function playMusic(name:String)
		{
			if (name == "Boss" && currentMusic != name)
			{
				if (music != null) music.stop();
				music = new Sfx(MUSIC_BOSS);
				music.loop(0.3);
			}
			else if (name == "Castle" && currentMusic != name)
			{
				if (music != null) music.stop();
				music = new Sfx(MUSIC_CASTLE);
				music.loop(0.3);
			}
			else if (name == "Save" && currentMusic != name)
			{
				if (music != null) music.stop();
				music = new Sfx(MUSIC_SAVE);
				music.loop(0.3);
			}
			else if (name == "Forest" && currentMusic != name)
			{
				if (music != null) music.stop();
				music = new Sfx(MUSIC_FOREST);
				music.loop(0.3);
			}
			
			currentMusic = name;
		}
		
		public function registerChest(chest:Chest)
		{
			var chestID:String = (FP.world as BaseWorld).currentName + chest.name;
			chests.push(chestID);
		}
		
		public function isChestValid(chest:Chest):Boolean
		{
			var chestID:String = (FP.world as BaseWorld).currentName + chest.name;
			if (chests.indexOf(chestID) == -1) return true;
			
			return false;
		}
	}

}