package game 
{
	import flash.display.SpreadMethod;
	import net.flashpunk.Entity;
	import net.flashpunk.*;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.utils.*;
	/**
	 * ...
	 * @author ...
	 */
	public class Chest extends Entity
	{
		[Embed(source = "../Assets/Chest.png")]
		private const CHEST:Class;
		
		public var worms:int;
		public var coins:int;
		public var healthUp:Boolean;
		private var animation:Spritemap;
		private var openTimer:Number;
		private var openned:Boolean;
		
		public function Chest(x:int, y:int, id:String) 
		{
			animation = new Spritemap(CHEST, 46, 46);
			animation.add("closed", [0]);
			animation.add("open", [0, 1, 2, 3], 10, false);
			animation.play("closed");
			addGraphic(animation);
			animation.alpha = 0;
			animation.centerOO();
			
			this.x = x;
			this.y = y;
			this.centerOrigin();
			
			setHitbox(46, 46, 23, 23);
			
			openTimer = 0;
			
			name = id;
			type = "chest";
		}
		
		override public function update():void 
		{
			super.update();
			
			if (!Stats.GetInstance().isChestValid(this) && openned == false)
			{
				FP.world.remove(this);
			}
			else
			{
				animation.alpha = 1;
			}
			
			while (!collide("ground", x, y + 1))
			{
				y += 1;
			}
			
			if (openned == true)
			{
				openTimer += FP.elapsed;
				if (openTimer > 2)
				{
					FP.world.remove(this);
				}
			}
		}
		
		public function open():void
		{
			if (openned == false)
			{
				openned = true;
				animation.play("open");
				Stats.GetInstance().registerChest(this);
				Stats.GetInstance().maxHealth += 20;
				Stats.GetInstance().health = Stats.GetInstance().maxHealth;
			}
		}
	}

}