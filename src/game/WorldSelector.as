package game 
{
	/**
	 * ...
	 * @author ...
	 */
	public class WorldSelector 
	{
		[Embed(source = "../levels/Test.tmx", mimeType = "application/octet-stream")]
		public static const TESTLEVEL:Class;
		
		[Embed(source = "../levels/Home.tmx", mimeType = "application/octet-stream")]
		public static const HOME:Class;
		
		[Embed(source = "../levels/Town.tmx", mimeType = "application/octet-stream")]
		public static const TOWN:Class;
		
		[Embed(source = "../levels/ForestZone1.tmx", mimeType = "application/octet-stream")]
		public static const FOREST1:Class;
		
		[Embed(source = "../levels/ForestZone2.tmx", mimeType = "application/octet-stream")]
		public static const FOREST2:Class;
		
		[Embed(source = "../levels/ForestZone3.tmx", mimeType = "application/octet-stream")]
		public static const FOREST3:Class;
		
		[Embed(source = "../levels/Castle0.tmx", mimeType = "application/octet-stream")]
		public static const CASTLE0:Class;
		
		[Embed(source = "../levels/Castle1.tmx", mimeType = "application/octet-stream")]
		public static const CASTLE1:Class;
		
		[Embed(source = "../levels/Castle2.tmx", mimeType = "application/octet-stream")]
		public static const CASTLE2:Class;
		
		[Embed(source = "../levels/Castle3.tmx", mimeType = "application/octet-stream")]
		public static const CASTLE3:Class;
		
		[Embed(source = "../levels/Castle4.tmx", mimeType = "application/octet-stream")]
		public static const CASTLE4:Class;
		
		[Embed(source = "../levels/CastleBoss.tmx", mimeType = "application/octet-stream")]
		public static const CASTLEBOSS:Class;
		
		[Embed(source = "../levels/Save1.tmx", mimeType = "application/octet-stream")]
		public static const SAVE1:Class;
		
		[Embed(source = "../levels/Save2.tmx", mimeType = "application/octet-stream")]
		public static const SAVE2:Class;
		
		[Embed(source = "../levels/Save3.tmx", mimeType = "application/octet-stream")]
		public static const SAVE3:Class;
		
		public static function levelByName(name:String):Class
		{
			if (name == "test") return TESTLEVEL;
			if (name == "home") return HOME;
			if (name == "town") return TOWN;
			if (name == "forest1") return FOREST1;
			if (name == "forest2") return FOREST2;
			if (name == "forest3") return FOREST3;
			if (name == "castle0") return CASTLE0;
			if (name == "castle1") return CASTLE1;
			if (name == "castle2") return CASTLE2;
			if (name == "castle3") return CASTLE3;
			if (name == "castle4") return CASTLE4;
			if (name == "castleboss") return CASTLEBOSS;
			
			if (name == "save1") return SAVE1;
			if (name == "save2") return SAVE2;
			if (name == "save3") return SAVE3;
			
			return TESTLEVEL;
		}
		
	}

}