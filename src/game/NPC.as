package game 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	/**
	 * ...
	 * @author ...
	 */
	public class NPC extends Entity
	{
		[Embed(source = "../Assets/NPC1.png")]
		private const NPC1:Class;
		
		[Embed(source = "../Assets/NPC2.png")]
		private const NPC2:Class;
		
		public function NPC(tx:int, ty:int) 
		{
			var image:Image = new Image((Math.random() > 0.5) ? NPC1 : NPC2);
			image.originX = 64;
			image.originY = 64;
			addGraphic(image);
			image.flipped = ((Math.random() > 0.5) ? true : false);
			
			setHitbox(28, 46, 14, 46);
			
			this.x = tx;
			this.y = ty;
		}
		
		override public function update():void 
		{
			super.update();
			
			while (!collide("ground", x, y))
			{
				y += 1;
			}
		}
		
	}

}