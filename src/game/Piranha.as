package game 
{
	import net.flashpunk.*;
	import net.flashpunk.graphics.Anim;
	import net.flashpunk.graphics.Spritemap;
	/**
	 * ...
	 * @author ...
	 */
	public class Piranha extends BaseEntity
	{
		[Embed(source = "../Assets/Piranha.png")]
		private const PIRANHA:Class;
		
		private var animation:Spritemap;
		
		private var damageTime:Number;
		private var hitDirection:int;
		
		private var foundPlayer:Boolean;
		private var deathTimer:Number;
		
		public function Piranha() 
		{
			animation = new Spritemap(PIRANHA, 64, 64);
			animation.add("stand", [0, 1, 2], 10, true);
			animation.add("walk", [0, 1, 2], 10, true);
			animation.add("attack", [0, 1, 2], 20, true);
			
			animation.play("stand");
			
			animation.originX = 32;
			animation.originY = 64;
			addGraphic(animation as Graphic);
			
			setHitbox(20, 20, 10, 20);
			
			health = 20;
			damageTime = 0;
			
			type = "enemy";
			
			xVel = yVel = 0;
		}
		
		override public function update():void 
		{
			super.update();
			
			var player:Player = (FP.world as BaseWorld).player;
			
			if (health > 0)
			{
				if (damageTime > 0)
				{
					xVel = (300 - HeckMath.easeOut(0, 200, (0.4 - damageTime) / 0.4)) * hitDirection;
					damageTime -= FP.elapsed;
					if (damageTime <= 0)
					{
						animation.play("stand");
					}
				}
				else
				{
					// AI
					
					if (FP.distance(player.x, player.y, x, y) < 8*32)
					{
						foundPlayer = true;
						animation.play("walk");
					}
				}
				
				if (damageTime > 0)
				{
					animation.alpha = 0.5;
				}
				else
				{
					animation.alpha = 1;
				}
			}
			else
			{
				deathTimer += FP.elapsed;
				animation.alpha = FP.lerp(1, 0, deathTimer / 1);
				if (deathTimer > 1)
				{
					FP.world.remove(this);
				}
			}
			
			yVel += 450 * FP.elapsed;
			
			if (collide("ground", x, y+1) && yVel >= 0)
			{
				while (collide("ground", x, y))
				{
					y -= 1;
				}
				
				yVel = FP.random * -280;
				
				if (foundPlayer && Math.abs(player.x - x) > 10)
				{
					xVel = player.x - x;
					xVel /= Math.abs(xVel);
					xVel *= 180;
					
					if (xVel < 0) animation.flipped = false;
					else animation.flipped = true;
				}
			}
			if (collide("ground", x + xVel * FP.elapsed, y))
			{
				xVel = 0;
			}
			if (collide("ground", x, y - 1) && yVel < 0)
			{
				yVel = 0;
				
				while (collide("ground", x, y))
				{
					y += 1;
				}
			}
			
			x += xVel * FP.elapsed;
			y += yVel * FP.elapsed;
			
			if (damageTime <= 0)
			{
				var coll:Player = collide("player", x, y) as Player;
				if (coll != null)
				{
					coll.damage(7, this);
				}
			}
			
			if (x > (FP.world as BaseWorld).worldWidth - 10)
			{
				x = (FP.world as BaseWorld).worldWidth - 10
			}
			if (x < 10)
			{
				x = 10;
			}
		}
		
		override public function damage(damage:int, attacker:Entity):void
		{
			super.damage(damage, attacker);
			
			animation.play("walk");
			damageTime = 0.4;
			hitDirection = x - attacker.x;
			hitDirection = hitDirection / Math.abs(hitDirection);
			
			if (health <= 0)
			{
				collidable = false;
				setHitbox(0,-500,1,1);
				yVel = -200;
				deathTimer = 0;
				
				animation.play("stand");
			}
		}
	}

}