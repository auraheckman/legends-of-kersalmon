package game 
{
	import net.flashpunk.Entity;
	import net.flashpunk.*;
	import net.flashpunk.graphics.Spritemap;
	/**
	 * ...
	 * @author ...
	 */
	public class Boss extends BaseEntity
	{
		[Embed(source = "../Assets/King.png")]
		private const KING:Class;
		
		[Embed(source = "../sfx/playerhit.mp3")]
		private const PLAYER_HIT:Class;
		
		private var animation:Spritemap;
		private var startTimer:Number;
		public var flying:Boolean;
		private var protectionTimer:Number;
		private var mortalityTimer:Number;
		private var spawnTimer:Number;
		private var hitTimer:Number;
		public var deathTimer:Number;
		
		public function Boss() 
		{
			animation = new Spritemap(KING, 128, 64);
			animation.add("stand", [0], 10);
			animation.add("walk", [0, 1], 5);
			animation.add("attack", [0, 2, 3, 3, 3, 3, 2, 0], 20, false);
			animation.add("fall", [0, 4, 5, 6], 20, false);
			animation.play("walk");
			animation.originX = animation.originY = 64;
			addGraphic(animation);
			
			originX = originY = 64;
			setHitbox(32, 64, 16, 64);
			
			health = 2;
			
			startTimer = 4;
			spawnTimer = 0;
			hitTimer = 0;
			
			name = "Boss";
			type = "enemy";
		}
		
		override public function update():void 
		{
			super.update();
			
			
			
			// IGNORE
			if (startTimer > 0)
			{
				startTimer -= FP.elapsed;
				if (startTimer <= 0)
				{
					flying = true;
					xVel = -60;
				}
				return;
			}
			
			if (!flying)
			{
				y += 500 * FP.elapsed;
				animation.play("fall");
			}
			else
			{
				yVel = -80;
				
				if (animation.flipped && x > 370)
				{
					animation.play("walk");
					xVel = -60;
					animation.flipped = false;
				}
				else if (!animation.flipped && x < 30)
				{
					animation.play("walk");
					animation.flipped = true;
					xVel = 60;
				}
			}
			
			if (mortalityTimer > 0)
			{
				xVel = 0;
				mortalityTimer -= FP.elapsed;
				if (mortalityTimer <= 0)
				{
					animation.play("walk");
					flying = true;
					xVel = (FP.random > 0.5 ? -60 : 60);
					animation.flipped = (xVel < 0 ? false : true);
				}
			}
			else
			{
				spawnTimer += FP.elapsed;
				
				if (spawnTimer > 24)
				{
					animation.play("attack");
					spawnTimer = 0;
					for (var i:int = 0; i < 2; i++)
					{
						var enemy:BaseEntity = null;
						var chance:Number = FP.random;
						if (chance > 0.75)
						{
							enemy = new SquidKnight();
						}
						else if (chance > 0.5)
						{
							enemy = new Octoninja();
						}
						else if (chance > 0.25)
						{
							enemy = new Floundier();
						}
						else
						{
							enemy = new Piranha();
						}
						enemy.x = 30 + (FP.random * 260);
						enemy.y = 0;
						FP.world.add(enemy);
					}
				}
			}
			
			if (hitTimer > 0)
			{
				hitTimer -= FP.elapsed;
				animation.alpha = 0.5;
			}
			else
			{
				animation.alpha = 1;
			}
			
			x += xVel * FP.elapsed;
			y += yVel * FP.elapsed;
			
			if (y < 100) y = 100;
			if (y > 320 - 32) y = 320 - 32;
			
			if (deathTimer > 0)
			{
				deathTimer -= FP.elapsed;
				animation.alpha = FP.lerp(1, 0, (3 - deathTimer) / 3);
				if (deathTimer <= 0)
				{
					FP.world.remove(this);
				}
				
				mortalityTimer = 10;
			}
		}
		
		override public function damage(damage:int, attacker:Entity):void 
		{
			if (hitTimer <= 0 && mortalityTimer > 0)
			{
				health -= 1;
				hitTimer = 0.4;
				var sound:Sfx = new Sfx(PLAYER_HIT);
				sound.volume = 0.3;
				sound.play();
			}
		}
		
		public function mortalize()
		{
			mortalityTimer = 3;
			flying = false;
		}
	}

}