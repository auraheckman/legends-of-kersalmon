package game 
{
	import net.flashpunk.*;
	import net.flashpunk.graphics.Image;
	/**
	 * ...
	 * @author ...
	 */
	public class Shurikan extends Entity
	{
		[Embed(source = "../Assets/Shurikan.png")]
		private const SHURIKAN:Class;
		
		private var image:Image;
		private var xVel:int, yVel:int;
		private var life:Number;
		
		public function Shurikan(x:int, y:int, xVel:int, yVel:int) 
		{
			image = new Image(SHURIKAN);
			image.centerOO();
			addGraphic(image);
			
			this.x = x;
			this.y = y;
			this.xVel = xVel;
			this.yVel = yVel;
			
			centerOrigin();
			setHitbox(8, 8, 4, 4);
			
			life = 4;
		}
		
		override public function update():void 
		{
			super.update();
			
			image.angle = FP.angle(0, 0, xVel, yVel);
			
			x += xVel * FP.elapsed;
			y += yVel * FP.elapsed;
			
			life -= FP.elapsed;
			if (life <= 0)
			{
				FP.world.remove(this);
			}
			
			if (collide("ground", x, y))
			{
				FP.world.remove(this);
			}
			
			var player:BaseEntity = collide("player", x, y) as BaseEntity;
			if (player != null)
			{
				player.damage(20, this);
				FP.world.remove(this);
			}
		}
	}

}