package game 
{
	import net.flashpunk.FP;
	/**
	 * ...
	 * @author ...
	 */
	public class HeckMath 
	{
		
		public function HeckMath() 
		{
			
		}
		
		public static function easeOut(start:Number, end:Number, t:Number):Number {
			t = FP.clamp(t, 0, 1);
			return -end * t*(t-2) + start;
		};
	}

}