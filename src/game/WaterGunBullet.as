package game 
{
	import flash.events.IMEEvent;
	import net.flashpunk.*;
	import net.flashpunk.graphics.Image;
	/**
	 * ...
	 * @author ...
	 */
	public class WaterGunBullet extends Entity
	{
		[Embed(source = "../Assets/WaterGun.png")]
		private const WATER_GUN:Class;
		
		private var xVel:int, yVel:int;
		private var image:Image;
		private var life:Number;
		
		public function WaterGunBullet(x:int, y:int, direction:int, yVel:int ) 
		{
			this.x = x;
			this.y = y;
			
			this.yVel = yVel;
			xVel = Math.abs(yVel/2) * direction;
			life = 3;
			
			image = new Image(WATER_GUN);
			addGraphic(image);
			
			type = "playerdamage";
		}
		
		override public function update():void 
		{
			super.update();
			
			x += xVel * FP.elapsed;
			y += yVel * FP.elapsed;
			
			yVel += 450 * FP.elapsed;
			
			image.angle = FP.angle(0, 0, xVel, yVel);
			
			var enemy:BaseEntity = collide("enemy", x, y) as BaseEntity;
			if (enemy != null)
			{
				enemy.damage(13, this);
				FP.world.remove(this);
			}
			
			life -= FP.elapsed;
			if (life <= 0)
			{
				FP.world.remove(this);
			}
		}
		
	}

}