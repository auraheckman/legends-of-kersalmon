package game 
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Backdrop;
	import net.flashpunk.graphics.Image;
	/**
	 * ...
	 * @author ...
	 */
	public class Background extends Entity
	{
		
		public function Background(source:*, repeating:Boolean) 
		{
			layer = 10000;
			if (repeating == false)
			{
				var image:Image = new Image(source);
				image.scrollX = image.scrollY = 0;
				addGraphic(image);
			}
			else
			{
				var bg:Backdrop = new Backdrop(source, true, true);
				addGraphic(bg);
			}
		}
	}

}