package game 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.utils.*;
	import net.flashpunk.*;
	/**
	 * ...
	 * @author ...
	 */
	public class Dialog extends Entity
	{
		[Embed(source = "../Assets/Arrow.png")]
		private const ARROW:Class;
		
		public var text:String;
		public var face:Class;
		public var done:Boolean;
		public var time:Number;
		
		public var textGraphic:Text;
		public var arrow:Spritemap;
		
		public var next:Dialog;
		
		public function Dialog(text:String, faceSource:*) 
		{
			this.text = text;
			this.face = faceSource;
			
			time = 0;
			
			var rect:Image = Image.createRect(400, 96, 0x000000, 0.8);
			rect.scrollX = 0;
			rect.scrollY = 0;
			addGraphic(rect);
			
			textGraphic = new Text("", 10, 10);
			textGraphic.wordWrap = true;
			textGraphic.width = 380;
			textGraphic.richText = "";
			textGraphic.scrollX = 0;
			textGraphic.scrollY = 0;
			addGraphic(textGraphic);
			
			arrow = new Spritemap(ARROW, 16, 16);
			arrow.add("play", [0, 1], 3);
			arrow.play("play");
			arrow.x = 400 - 16;
			arrow.y = 96 - 16;
			arrow.scrollX = 0;
			arrow.scrollY = 0;
			addGraphic(arrow);
			
			type = "dialog";
		}
		
		override public function update():void 
		{
			super.update();
			
			time += 50 * FP.elapsed;
			if (time > text.length) time = text.length;
			
			textGraphic.richText = text.substr(0, time);
			
			if (next == null)
			{
				arrow.alpha = 0;
			}
			
			if (Input.pressed(Key.F))
			{
				if (next != null)
				{
					FP.world.add(next);
				}
				FP.world.remove(this);
			}
		}
	}

}