package game 
{
	import flash.display.Sprite;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	/**
	 * ...
	 * @author ...
	 */
	public class UI extends Entity
	{
		[Embed(source = "../Assets/FishUI.png")]
		private const FISH_UI:Class;
		
		public var fishUI:Spritemap;
		public var healthBar:Image;
		public var healthBarBG:Image;
		
		public function UI() 
		{
			fishUI = new Spritemap(FISH_UI, 75, 26);
			fishUI.add("koi", [0]);
			fishUI.add("archer", [1]);
			fishUI.add("snapper", [2]);
			fishUI.add("puffer", [3]);
			fishUI.play("koi");
			fishUI.scrollX = 0;
			fishUI.scrollY = 0;
			fishUI.y = 16;
			
			addGraphic(fishUI);
			
			healthBar = Image.createRect(1, 10, 0x00AA00);
			healthBar.x = 1;
			healthBar.y = 1;
			healthBar.scrollX = healthBar.scrollY = 0;
			healthBarBG = Image.createRect(1, 12, 0x000000);
			healthBarBG.scrollX = healthBarBG.scrollY = 0;
			
			addGraphic(healthBarBG);
			addGraphic(healthBar);
		}
		
		override public function update():void 
		{
			super.update();
			
			fishUI.play(Stats.GetInstance().currentFish);
			
			healthBar.scaleX = Stats.GetInstance().health-2;
			healthBarBG.scaleX = Stats.GetInstance().maxHealth;
		}
	}

}